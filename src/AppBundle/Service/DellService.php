<?php

namespace AppBundle\Service;

use AppBundle\Entity\Deal;
use AppBundle\Parser\BaseParser;
use AppBundle\Parser\Dell\Accessory;
use AppBundle\Parser\Dell\Audio;
use AppBundle\Parser\Dell\Monitor;
use AppBundle\Parser\Dell\Tv;
use GuzzleHttp\Client;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Cache\Adapter\TraceableAdapter;

class DellService
{
    /** @var string */
    private $baseUrl = 'http://www.dell.com/csbapi/en-us/anavfilter/GetSnPResults';

    /** @var Deal[] */
    private $deals = [];

    /** @var TraceableAdapter */
    private $cache;

    /**
     * DellService constructor.
     *
     * @param CacheItemPoolInterface $cache
     */
    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $filter
     *
     * @return Deal[]
     */
    public function getDeals($filter = 'tv')
    {
        $itemCache = $this->cache->getItem($filter);
        if ($itemCache->isHit()) {
            return $itemCache->get();
        }

        //Load deals from site
        $this->fetchDeals($filter);

        //Save it to the cache
        $itemCache->set($this->deals);
        $itemCache->expiresAfter(\DateInterval::createFromDateString('15 minutes'));
        $this->cache->save($itemCache);

        return $this->deals;
    }

    /**
     * Load the deals from the site
     *
     * @param string $filter
     */
    private function fetchDeals($filter)
    {
        $parser = $this->getParser($filter);
        $page = 1;
        while ($page) {
            $response = $this->getPage(
                $parser->getCategoryPath(),
                $parser->getCategoryId(),
                $parser->getParentCategoryId(),
                $page
            );
            if ($response) {
                $this->deals += $parser->parseDeal($response);
            }
        }

        //Sort the data
        $parser::sortDeals($this->deals);
    }

    /**
     * @param $categoryPath
     * @param $categoryId
     * @param $parentCategoryId
     * @param int $page
     * @return bool|mixed
     */
    public function getPage($categoryPath, $categoryId, $parentCategoryId, &$page = 1)
    {
        $params = [
            'query' => [
                'categorypath' => $categoryPath,
                'sortby' => 'numberofreviews-descending',
                'appliedRefinements' => '',
                'page' => $page,
                'categoryid' => $categoryId,
                'parentCategoryId' => $parentCategoryId,
                'isMerchandizingCategory' => 'False',
            ],
        ];

        $client = new Client();
        $response = $client->get(
            $this->baseUrl,
            $params
        );

        $responseJson = json_decode($response->getBody()->getContents());
        if (empty($responseJson->Pagination)) {
            $page = false;

            return false;
        }

        if (empty($responseJson->Pagination)
            || $responseJson->Pagination->CurrentPage
            >= $responseJson->Pagination->TotalPages) {
            $page = false;

            return $responseJson;
        }

        $page++;

        return $responseJson;
    }

    /**
     * @param $type
     * @return BaseParser
     * @throws \Exception
     */
    protected function getParser($type)
    {
        switch ($type) {
            case 'tv':
                return new Tv();
            case 'monitor':
                return new Monitor();
            case 'accessory':
                return new Accessory();
            case 'audio':
                return new Audio();
            default:
                throw new \Exception('Unable to locate the appropriate parser for '.$type);
        }
    }

    /**
     * @return array
     */
    public function getAvailableTypes()
    {
        return [
            'accessory' => 'Accessory',
            'audio' => 'Audio',
            'monitor' => 'Monitor',
            'tv' => 'Television',
        ];
    }
}
