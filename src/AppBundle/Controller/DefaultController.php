<?php

namespace AppBundle\Controller;

use AppBundle\Service\DellService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/{filter}", name="homepage")
     *
     * @param string $filter
     * @param DellService $dellService
     *
     * @return Response
     */
    public function indexAction(DellService $dellService, $filter = 'tv')
    {
        return $this->render(
            'default/index.html.twig',
            [
                'deals' => $dellService->getDeals($filter),
                'types' => $dellService->getAvailableTypes(),
                'filter' => $filter,
            ]
        );
    }
}
