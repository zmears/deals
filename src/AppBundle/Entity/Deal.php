<?php

namespace AppBundle\Entity;

class Deal
{
    /** @var string */
    public $sku;

    /** @var string */
    public $title;

    /** @var string */
    public $img;

    /** @var int */
    public $category;

    /** @var string */
    public $link;

    /** @var string */
    public $price;

    /** @var string */
    public $marketPrice;

    /** @var string */
    public $promo;

    /** @var boolean */
    public $highlight = false;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Deal
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param string $img
     *
     * @return Deal
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $category
     *
     * @return Deal
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     *
     * @return Deal
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     *
     * @return Deal
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param string $promo
     *
     * @return Deal
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHighlight(): bool
    {
        return $this->highlight;
    }

    /**
     * @param bool $highlight
     *
     * @return Deal
     */
    public function setHighlight(bool $highlight)
    {
        $this->highlight = $highlight;

        return $this;
    }

    /**
     * @return string
     */
    public function getMarketPrice()
    {
        return $this->marketPrice;
    }

    /**
     * @param string $marketPrice
     *
     * @return Deal
     */
    public function setMarketPrice($marketPrice)
    {
        $this->marketPrice = $marketPrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     *
     * @return Deal
     */
    public function setSku(string $sku)
    {
        $this->sku = $sku;

        return $this;
    }
}
