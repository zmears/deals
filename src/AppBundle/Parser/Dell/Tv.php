<?php

namespace AppBundle\Parser\Dell;

use AppBundle\Entity\Deal;
use AppBundle\Parser\BaseParser;

class Tv extends BaseParser
{
    /** @var int */
    protected $categoryPath = 8224;

    /** @var int */
    protected $categoryId = 8224;

    /** @var int */
    protected $parentCategoryId = 8223;

    /**
     * @param \stdClass $response
     *
     * @return Deal[]
     */
    public function parseDeal(\stdClass $response)
    {
        $deals = [];
        if (!empty($response->Results->Stacks)) {
            foreach ($response->Results->Stacks as $stack) {
                // Skip any records that don't have a Sku. They aren't valid.
                if (!$stack->Stack->Sku->Value) {
                    continue;
                }

                $deal = new Deal();

                $deal->setSku($stack->Stack->Sku->Value)
                    ->setTitle($stack->Stack->Title->Value)
                    ->setImg($stack->Stack->ProductImage->ImageUri)
                    ->setCategory($stack->Stack->CategoryInfo)
                    ->setLink('http://www.dell.com'.$stack->Stack->Links->ViewDetailsLink->Url)
                    ->setPrice($stack->Stack->Pricing->DellPrice->Value)
                    ->setMarketPrice($stack->Stack->Pricing->MarketValue->Value);

                if (!empty($stack->Stack->Pricing->SpecialOffersDetails->Details)) {
                    $promo = array_pop($stack->Stack->Pricing->SpecialOffersDetails->Details);
                    $deal->setPromo($promo->Description);
                }

                $title = $deal->getTitle();

                if (stripos($title, '55 inch') !== false || stripos($title, '55"') !== false) {
                    $deal->setHighlight(true);
                }

                $deals[$deal->getSku()] = $deal;
            }
        }

        return $deals;
    }

    /**
     * @param Deal[] $deals
     */
    public static function sortDeals(&$deals)
    {
        usort($deals, function (Deal $a, Deal $b) {
            return $b->getPromo() <=> $a->getPromo();
        });
    }
}
