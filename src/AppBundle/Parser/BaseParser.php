<?php

namespace AppBundle\Parser;

use AppBundle\Entity\Deal;

abstract class BaseParser implements Parser
{
    /** @var int */
    protected $categoryPath;

    /** @var int */
    protected $categoryId;

    /** @var int */
    protected $parentCategoryId;

    /**
     * @param \stdClass $response
     * @return Deal[]
     */
    abstract public function parseDeal(\stdClass $response);

    /**
     * @return int
     * @throws \Exception
     */
    public function getCategoryPath()
    {
        if (empty($this->categoryPath)) {
            throw new \Exception('Category Path not set for '.__CLASS__);
        }

        return $this->categoryPath;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getCategoryId()
    {
        if (empty($this->categoryId)) {
            throw new \Exception('Category Id not set for '.__CLASS__);
        }

        return $this->categoryId;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getParentCategoryId()
    {
        if (empty($this->parentCategoryId)) {
            throw new \Exception('Parent Category Id not set for '.__CLASS__);
        }

        return $this->parentCategoryId;
    }

    /**
     * @param Deal[] $deals
     *
     * @return void
     */
    abstract public static function sortDeals(&$deals);
}
