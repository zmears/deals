<?php

namespace AppBundle\Parser;

interface Parser
{
    /**
     * @param \stdClass $response
     *
     * @return array
     */
    public function parseDeal(\stdClass $response);

    /**
     * @return int
     */
    public function getCategoryPath();

    /**
     * @return int
     */
    public function getCategoryId();

    /**
     * @return int
     */
    public function getParentCategoryId();
}
